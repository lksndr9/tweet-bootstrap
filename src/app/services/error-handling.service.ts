import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {

  constructor(
    private toastr: ToastrService
  ) { }
  
  showSuccess (text: string) {
    this.toastr.info(text,'', {timeOut: 2000});
  }
  
  showError (text: string) {
    this.toastr.error(text,'', {timeOut: 2000});
  }

  showWarrning(text: string) {
    this.toastr.warning(text,'', {timeOut: 2000});
  }
}
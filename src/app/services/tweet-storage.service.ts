import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import { TweetObjectModel } from '../models/tweet-object.model';
import { TweetStorageInterface } from '../interfaces/tweet-storage.interface';
import { DataBaseCommunicationModel } from '../models/database-communication';
import { Router } from '@angular/router';

//* Service for backend simulator  *//

@Injectable({
  providedIn: 'root'
})

export class TweetStorageService implements TweetStorageInterface {

  constructor(
    private router: Router
  ) { }

  // Read all users data from storage
  getUserDataStorage(key: string): UserModel[] {
    const valueOfStorage: string | null = localStorage.getItem(key);
    return valueOfStorage === null ? null : JSON.parse(valueOfStorage);
  }

  // Save user data to storage
  saveUserDataStorage(key: string,data: UserModel[]): void {
    localStorage.setItem(key, JSON.stringify(data)); 
  }

  // Delete user data from storage
  deleteUserDataStorage(key: string): void {
    localStorage.removeItem(key);
  }

  // Get paged tweets and total count of tweets for paginator
  getTweets (pageSize: number, currentPage: number): DataBaseCommunicationModel {

    // Declare empty array of communication objects
    let pagedTweet: DataBaseCommunicationModel = {} as DataBaseCommunicationModel;
    
    // Read from local storage
    const valueOfStorage: string | null | undefined = localStorage.getItem('tweets');
    
    // If local storage is undefined/ null / does'nt exist, array is empty 
    if (valueOfStorage?.length === 0 || valueOfStorage === null || valueOfStorage === undefined) { 
      pagedTweet.tweetsPage = [];
    return pagedTweet }
    
    // Parse string from storage and get total length for paginator
    let allTweets: TweetObjectModel[] = JSON.parse(valueOfStorage);
    const count = allTweets.length;
    if (allTweets == null || allTweets === undefined) {return pagedTweet }
    
    // Reverse array so last writen message are on first page
    allTweets = allTweets.reverse();
    
    // Get right slice of array 
    let newArray = allTweets.slice((pageSize*currentPage),(pageSize*currentPage)+pageSize);
    
    // Prepare for send
    pagedTweet.tweetsPage = newArray;
    pagedTweet.count = count;
    
    // Return page with total count of tweets
    return pagedTweet;
  }
  
  saveTweet(data: TweetObjectModel): void {

    let storageTweets: TweetObjectModel[] = [];

    // Read all tweets from storage
    const valueOfStorage: string | null |undefined = localStorage.getItem('tweets');
    if (valueOfStorage?.length === 0 || valueOfStorage === null || valueOfStorage === undefined) { storageTweets = [] }
    if (valueOfStorage !== null){ storageTweets = JSON.parse(valueOfStorage)};
    
    // Add new tweet
    storageTweets.push(data);
    
    // Save to storage
    localStorage.setItem('tweets', JSON.stringify(storageTweets)); 
  }

  deleteTweet(tweet: TweetObjectModel): void {

    // Get array from storage
    let storageTweets: TweetObjectModel[] = [];
    
    // Read all tweets from storage
    const valueOfStorage: string | null | undefined = localStorage.getItem('tweets');
    if (valueOfStorage?.length === 0 || valueOfStorage === null || valueOfStorage === undefined) { storageTweets = [] }
    if (valueOfStorage !== null){ storageTweets = JSON.parse(valueOfStorage)}
    
    // Find tweet in array by time it is writen
    let index = storageTweets.findIndex( element => element.writenAt === tweet.writenAt);
    
    // Delete tweet from array
    storageTweets.splice(index,1);
    
    // Write array to storage
    localStorage.setItem('tweets', JSON.stringify(storageTweets));
  }

  getActiveTweetUsers(): string[] {

    let userList: string[] = [];
    let storageTweets: TweetObjectModel[] = [];
    
    // Read all tweets from storage
    const valueOfStorage: string | null |undefined = localStorage.getItem('tweets');
    if (valueOfStorage?.length === 0 || valueOfStorage === null || valueOfStorage === undefined) { storageTweets = [] }
    if (valueOfStorage !== null){ storageTweets = JSON.parse(valueOfStorage)};

    // Make new array of all usernames who wrote in this tweet
    storageTweets.forEach ( element => {
      userList.push(element.username)
    })

    // Return unique usernames who wrote tweets
    return userList.map(item => item).filter((value, index, self) => self.indexOf(value)===index);
  }
}

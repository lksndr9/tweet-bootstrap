import { Injectable } from '@angular/core';
import { TweetObjectModel } from '../models/tweet-object.model';
import { DataBaseCommunicationModel } from '../models/database-communication';
import { TweetStorageInterface } from '../interfaces/tweet-storage.interface';

//* Service for cummunication with backend   *//

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  
  // Service for communication with backend
  constructor(
    private tweetStorageInterface: TweetStorageInterface) { }
  
  // Get paged tweet from source 
  getPagedTweets (pageSize: number, currentPage: number): DataBaseCommunicationModel {
    // Send page size and current page to get chunk of tweets
    const pagedTweet: DataBaseCommunicationModel = this.tweetStorageInterface.getTweets(pageSize, currentPage);
    // If source is empty, make it empty array 
    if (pagedTweet.tweetsPage === undefined) { pagedTweet.tweetsPage = [] }
    return pagedTweet;
  }

  // Send tweet to be saved
  saveTweet(data: TweetObjectModel): void {
    this.tweetStorageInterface.saveTweet(data); 
  }

  // Delete specific tweet
  deleteTweet(tweet: TweetObjectModel): void {
    this.tweetStorageInterface.deleteTweet(tweet);
  }
  
  // Get active users from tweet
  getActiveUsers(): string[] {
    const active = this.tweetStorageInterface.getActiveTweetUsers();
    return active
  }
}

import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from '../components/confirmation/confirmation.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationModalService {

  constructor(
    private modalService: NgbModal
  ) { }

  public openConfirmationModal(message: string): Promise<boolean> {
    const modalRef = this.modalService.open(ConfirmationComponent, {size: 'sm', backdrop: 'static'});
    modalRef.componentInstance.message = message;
    modalRef.result.then(
      // ModalRef.result is Promise so have to be treated this way
      (result: Promise<boolean>)=> {
        return result;
      }, ()=>{return false}
    )
    // Method must return something in every case 
    return modalRef.result;
  }
}
import { Injectable } from "@angular/core";

@Injectable({providedIn: 'root'})

export abstract class ConfirmationInterface {

  abstract openConfirmationModal(message: string): Promise<boolean>;
}

import { Injectable } from "@angular/core";
import { UserModel } from "../models/user.model";

//* Interface of service for communicate with backend about users data  *//

@Injectable({providedIn: 'root'})
export abstract class UserStoreInterface {

  abstract userWrite (userData: UserModel): void
  abstract userGet () :UserModel;
  abstract userDelete(): void;
}
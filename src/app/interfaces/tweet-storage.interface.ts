import { Injectable } from "@angular/core";
import { TweetObjectModel } from "../models/tweet-object.model";
import { UserModel } from "../models/user.model";
import { DataBaseCommunicationModel } from '../models/database-communication';

//* Interface of service for backend simulator   *//

@Injectable({providedIn: 'root'})
export abstract class TweetStorageInterface {

  abstract getUserDataStorage(key: string): UserModel[];
  abstract saveUserDataStorage(key: string, data: UserModel[]): void;
  abstract deleteUserDataStorage(key: string): void;
  abstract getTweets (pageSize: number, currentPage: number): DataBaseCommunicationModel;
  abstract saveTweet(data: TweetObjectModel): void;
  abstract deleteTweet(tweet: TweetObjectModel): void;
  abstract getActiveTweetUsers(): string[];
}
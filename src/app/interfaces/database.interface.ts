import { Injectable } from "@angular/core";
import { DataBaseCommunicationModel } from '../models/database-communication';
import { TweetObjectModel } from "../models/tweet-object.model";

//* Interface of service for cummunicate with backend   *//

@Injectable({providedIn: 'root'})
export abstract class DatabaseInterface {

  abstract getPagedTweets (pageSize: number, currentPage:number): DataBaseCommunicationModel;
  abstract saveTweet(data: TweetObjectModel): void;
  abstract deleteTweet(tweet:TweetObjectModel): void;
  abstract getActiveUsers(): string[];
}
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { LoginWrapComponent } from './components/login-wrap/login-wrap.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TweetStorageInterface } from './interfaces/tweet-storage.interface';
import { TweetStorageService } from './services/tweet-storage.service';
import { DatabaseInterface } from './interfaces/database.interface';
import { DatabaseService } from './services/database.service';
import { UserStoreInterface } from './interfaces/user-store.interface';
import { UserStoreService } from './services/user-store.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputComponent } from './components/input/input.component';
import { TweetDivComponent } from './components/input/tweet-div/tweet-div.component';
import { UsersDivComponent } from './components/input/users-div/users-div.component';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationInterface } from './interfaces/confirmation.interface';
import { ConfirmationModalService } from './services/confirmation-modal.service';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginWrapComponent,
    LoginComponent,
    RegisterComponent,
    InputComponent,
    TweetDivComponent,
    UsersDivComponent,
    ConfirmationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgbModule,
    NgbModalModule,
  ],
  // entryComponents: [
  // ],
  providers: [
    {provide: TweetStorageInterface, useClass: TweetStorageService},
    {provide: DatabaseInterface, useClass: DatabaseService},
    {provide: UserStoreInterface, useClass: UserStoreService},
    {provide: ConfirmationInterface, useClass: ConfirmationModalService},

  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-wrap',
  templateUrl: './login-wrap.component.html',
  styleUrls: ['./login-wrap.component.scss']
})
export class LoginWrapComponent implements OnInit {

  constructor() {}

  // Switch flag
  isLogin: boolean = true;

  // Label for button
  buttonLabel: string = 'New here? REGISTER!';

  ngOnInit(): void {}
  
  // Choose login or register 
  rotateLogin() {
    this.isLogin=!this.isLogin;
    this.buttonLabel = this.isLogin? 'New here? REGISTER!' : 'Have account? LOGIN!';

  }
}

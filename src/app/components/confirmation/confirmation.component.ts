import { Component, Input } from '@angular/core';
import { NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent {

  // Message for modal
  @Input() public message = '' as string;

  modalRef?: NgbModalRef;

  constructor(
    public activeModal: NgbActiveModal,) {}
 
  // User answer
  confirm(): void {
    this.activeModal.close(true);
  }
 
  decline(): void {
    this.activeModal.close(false);
  }
}
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DatabaseInterface } from 'src/app/interfaces/database.interface';
import { UserStoreInterface } from 'src/app/interfaces/user-store.interface';
import { TweetObjectModel } from 'src/app/models/tweet-object.model';
import { UserModel } from 'src/app/models/user.model';
import { ConfirmationInterface } from '../../../interfaces/confirmation.interface';
import { ErrorHandlingService } from '../../../services/error-handling.service';

@Component({
  selector: 'app-tweet-div',
  templateUrl: './tweet-div.component.html',
  styleUrls: ['./tweet-div.component.scss']
})
export class TweetDivComponent implements OnInit {

// Forming of formgroup for input
inputFormGroup: FormGroup = new FormGroup ({tweet: new FormControl ('')})

// Current user loged in
currentUser: UserModel = {} as UserModel;

// Pipe for date transform
pipe = new DatePipe('en-US');

// Paginator initial parameters
pageSize = 5;
currentPage = 0;
totalRows = this.dataBase.getPagedTweets(this.pageSize,this.currentPage).count;

// Reset tweets page
tweetsAll: TweetObjectModel[] = [];

constructor(
  private userStoreInterface: UserStoreInterface,
  private router: Router,
  private dataBase: DatabaseInterface,
  private confirmation: ConfirmationInterface,
  private errorHandling: ErrorHandlingService
  ) {}
  
  ngOnInit(): void {
  console.log(this.totalRows);
  
  // Get paged source of tweets
  this.tweetsAll = this.dataBase.getPagedTweets(this.pageSize,(this.currentPage - 1)).tweetsPage;
  // Render table
  this.refreshTable();
  // Get current user
  this.currentUser = this.userStoreInterface.userGet();
  // If user is not logged in, go to login page
  if (Object.keys(this.currentUser).length === 0) {this.router.navigate(['login']);}
}

get tweet(): FormControl {return this.inputFormGroup.get('tweet') as FormControl}
tweetInvalid(): boolean {return this.tweet.invalid && (this.tweet.dirty || this.tweet.touched)}

onSubmit() {
  
  // New tweet object writen by user to be stored in array
  const newTweet: TweetObjectModel = {
    username: this.currentUser.username,
    body: this.tweet.value,
    writenAt: new Date
  }
  
  // If message is empty, do nothing
  if (newTweet.body === '') {return}
  
  // If array is null, let it be empty array
  if (this.tweetsAll === null) { this.tweetsAll = []}
  
  // Save in storage
  this.dataBase.saveTweet(newTweet);
  
  // Make input field empty in case of multiple clicks...
  this.tweet.setValue('');

  // Return to first page of tweets
  this.currentPage = 0;
  
  // Render table
  this.refreshTable();
  this.currentUser = this.userStoreInterface.userGet();
}

// Transform date for display
convertDate(date: Date): string | null {
  return this.pipe.transform(date, 'd MMMM, y, h:mm:ss a');
}

refreshTable() {
  // Read all tweets from local storage
  let databaseResp = this.dataBase.getPagedTweets(this.pageSize,(this.currentPage - 1));

  // Take page for display
  this.tweetsAll = databaseResp.tweetsPage;
  
  // Get total number of tweets to paginator
  this.totalRows = databaseResp.count;
}

// Read selected page
onPageChange(): void {
  this.refreshTable();
};

// Delete tweet from array (must be yours)
deleteTweet(tweet: TweetObjectModel) {
  this.dataBase.deleteTweet(tweet);
  this.refreshTable();
}

// Delete confirmation
confirmDelete(tweet: TweetObjectModel): void {
  const message = `Are you sure you want to delete this tweet?`;
  this.confirmation.openConfirmationModal(message).then(
    (result)=> {
      if (result === true) {
        this.deleteTweet(tweet);
        this.errorHandling.showSuccess('Tweet deleted!')
      }
      else { this.errorHandling.showWarrning('Canceled'); 
      }
    },
  )
}
}
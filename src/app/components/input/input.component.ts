import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { Router } from '@angular/router';
import { UserStoreInterface } from '../../interfaces/user-store.interface';
import { UserStoreService } from 'src/app/services/user-store.service';
import { ConfirmationInterface } from 'src/app/interfaces/confirmation.interface';
import { ErrorHandlingService } from 'src/app/services/error-handling.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit  {

  constructor(
    private userStoreInterface: UserStoreInterface,
    private userStoreService: UserStoreService,
    private router: Router,
    private confirmation: ConfirmationInterface,
    private errorHandling: ErrorHandlingService
    ) {}
    
  ngOnInit(): void {}

  onSubmit() {}

  // Clean current user data and send to login page
  logOut() {
    this.userStoreService.currentUser = {} as UserModel;
    this.userStoreInterface.userDelete();
    this.router.navigate(['login']);
  }

  // Log out confirmation
  confirmLogOut(): void {
    const message = `Are you sure you want to log out?`;
    this.confirmation.openConfirmationModal(message).then(
      (result)=> {
        if (result === true) {
          this.logOut();
        }
        else { this.errorHandling.showWarrning('Canceled'); 
        }
      },
    )
}
}
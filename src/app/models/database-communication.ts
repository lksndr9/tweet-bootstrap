import { TweetObjectModel } from './tweet-object.model';

//* Model for cummunication with backend   *//

export interface DataBaseCommunicationModel {
  count: number;
  pageSize: number;
  offset: number;
  tweetsPage: TweetObjectModel[];
}
//* Model for tweet  *//

export interface TweetObjectModel {
  username: string;
  body: string;
  writenAt: Date;
}
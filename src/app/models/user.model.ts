//* Model for user data  *//

export interface UserModel {
  email: string,
  password: string,
  username: string;
  firstName: string;
  lastName: string;
  phone: string;
  createdAt: Date
}
